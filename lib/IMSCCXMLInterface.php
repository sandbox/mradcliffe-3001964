<?php

/**
 * @file
 * IMS Global Content Package XML interface.
 */

/**
 * Provide a structured interface for reading and writing files.
 *
 * IMS Global Content Package XML files such as manifest or resource
 * XML files.
 */
interface IMSCCXMLInterface {

  /**
   * Returns an array of schema files to use to validate the document.
   *
   * @return array
   *   An indexed array of schema file locations.
   */
  public static function schema();

  /**
   * Returns DOMDocument::saveXML().
   *
   * @todo deprecate and change to getXml().
   */
  public function getXML();

  /**
   * Loads a DOM Document into the object from a file path.
   *
   * @param string $file_path
   *   The full file path to load.
   */
  public static function loadDocumentFromFile($file_path);

  /**
   * Parse the document into an array.
   */
  public function parseDocument();

  /**
   * Set the document. Should only be useful for Resource implementations.
   */
  public function setDocument();

  /**
   * Add elements from associative array. This should be a recursive function.
   *
   * @param array $document
   *   The document to set the structure for.
   * @param DOMElement $parent
   *   The DOMElement to append the structure to.
   * @param array $info
   *   An associative array of properties and children where the properties are
   *     - #name: The node name.
   *     - #value: An optional node value.
   *     - #attributes: An associative array of key/value attribute pairs.
   *     - #namespace: An optional array with a uri and name key.
   */
  public function setStructure($document, $parent, $info);

  /**
   * Get an element as a structure.
   *
   * The element name is the key and the value is a string or associative
   * array.
   *
   * @param DOMElement $element
   *   A DOMElement object.
   *
   * @return array
   *   An associative array of elements and properties.
   */
  public function getStructure($element);

  /**
   * Get an element from its XPath.
   *
   * @param string $path
   *   The XPath to lookup.
   *
   * @return DOMElement
   *   The DOMElement found or FALSE.
   */
  public function getElementByXPath($path);

  /**
   * Validate the document against all defined schema in implementation.
   *
   * @return array
   *   An indexed array of errors. If no errors, then an empty array.
   */
  public function validate();

}
