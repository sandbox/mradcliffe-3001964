<?php

/**
 * @file
 * IMS Global Learning Tool Interoperability Link Resource class.
 */

/**
 * LTI Link can be exported as XML and is a resource.
 *
 * IMS Global does not support including OAuth consumer information in a Basic
 * LTI Link resource. An end user must set this after import into their LMS.
 */
class IMSCCLTILink extends IMSCCResource {

  static public $type = 'imsbasiclti_xmlv1p0';
  static public $name = 'cartridge_basiclti_link';

  protected $bltins = 'http://www.imsglobal.org/xsd/imsbasiclti_v1p0';
  protected $lticpns = 'http://www.imsglobal.org/xsd/imslticp_v1p0';
  protected $lticmns = 'http://www.imsglobal.org/xsd/imslticm_v1p0';
  protected $xmlns = 'http://www.imsglobal.org/xsd/imslticc_v1p0';

  // public $title;
  public $description = '';
  public $launch_url;
  public $secure_launch_url = '';
  public $icon_url;
  public $secure_icon_url = '';
  public $custom = array();
  public $extensions = array();
  public $vendor = array('code' => '', 'name' => '');

  /**
   * Get static type.
   *
   * @return string
   *   The IMS Global Package file type.
   */
  public function getTypeAttribute() {
    return self::$type;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema() {
    $path = drupal_get_path('module', 'imscc');
    return array(
      $path . '/schema/imslticc_v1p0.xsd',
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function loadDocumentFromFile($file_path) {
    $instance = parent::loadDocumentFromFile($file_path);
    $instance->resource = $instance->parseDocument();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function setDocument() {
    parent::setDocument();

    // Add root element.
    $root = $this->document->createElementNS($this->xmlns, 'cartridge_basiclti_link');
    $schema_location = 'http://www.imsglobal.org/xsd/imslticc_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticc_v1p0.xsd';
    $schema_location .= ' http://www.imsglobal.org/xsd/imsbasiclti_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsbasiclti_v1p0p1.xsd';
    $schema_location .= ' http://www.imsglobal.org/xsd/imslticm_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticm_v1p0.xsd';
    $schema_location .= ' http://www.imsglobal.org/xsd/imslticp_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticp_v1p0.xsd';

    $root
      ->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', $schema_location);

    $this->document->appendChild($root);

    // Create LTI link.
    $this->createLTILink();
  }

  /**
   * Add LTI link info to the object.
   *
   * @param array $values
   *   An associative array of LTI information in the following format:
   *    - title:
   *    - description:
   *    - custom: an associative array of custom properties.
   *    - launch_url: An optional launch URL. If not defined, $base_url/lti.
   *    - icon: An optional url to an icon.
   *    - vendor: an associative array of vendor properties:
   *       - code: vendor.com
   *       - name: vendor name
   *       - description: A description of the vendor.
   *       - url: vendor url
   *       - contact: an associative array with an email key and value.
   * @param bool $https
   *   Launch URL should be secure. Default to TRUE because you really should
   *   NOT be using a non-secure URL.
   *
   * @return IMSCCLTILink
   *   Return the object again.
   */
  public function addLTIInfo($values, $https = TRUE) {
    global $base_url;

    $this->title = $values['title'];
    $this->description = isset($values['description']) ? $values['description'] : '';

    $this->launch_url = isset($values['launch_url']) ? $values['launch_url'] : $base_url . '/lti';

    if ($https && file_uri_scheme($this->launch_url) <> 'https') {
      // Include separate HTTPS url.
      $this->secure_launch_url = 'https://' . file_uri_target($this->launch_url);
    }
    elseif ($https) {
      $this->secure_launch_url = $this->launch_url;
    }

    $this->icon_url = isset($values['icon']) ? $values['icon'] : '';

    if ($https && !empty($this->icon_url) && file_uri_scheme($this->icon_url) <> 'https') {
      // Include separate HTTPs icon url.
      $this->secure_icon_url = 'https://' . file_uri_target($this->icon_url);
    }
    elseif ($https && !empty($this->icon_url)) {
      $this->secure_icon_url = $this->icon_url;
    }

    if (!empty($values['custom'])) {
      $this->custom = $values['custom'];
    }

    if (!empty($values['extensions'])) {
      $this->extensions = $values['extensions'];
    }

    $this->vendor = $values['vendor'];

    return $this;
  }

  /**
   * Create LTI Link Resource.
   *
   * All elements need to have check_plain() run through their values in order
   * to not produce invalid XML.
   *
   * The sequence of elements is strict.
   */
  public function createLTILink() {
    $blti_defaults = array(
      '#namespace' => array('uri' => $this->bltins, 'name' => 'blti'),
    );
    $lticm_defaults = array(
      '#namespace' => array('uri' => $this->lticmns, 'name' => 'lticm'),
    );
    $lticp_defaults = array(
      '#namespace' => array('uri' => $this->lticpns, 'name' => 'lticp'),
    );

    $title_info = $blti_defaults + array('#name' => 'title', '#value' => check_plain($this->title));
    $this->setStructure($this->document, $this->document->firstChild, $title_info);

    if (!empty($this->description)) {
      // Description is an optional element.
      $desc_info = $blti_defaults + array('#name' => 'description', '#value' => check_plain($this->description));
      $this->setStructure($this->document, $this->document->firstChild, $desc_info);
    }

    if (!empty($this->custom)) {
      $custom_info = $blti_defaults + array('#name' => 'custom');
      foreach ($this->custom as $key => $value) {
        // Add each custom parameter.
        $custom_info[] = $lticm_defaults + array(
          '#name' => 'property',
          '#value' => check_plain($value),
          '#attributes' => array(
            'name' => $key,
          ),
        );
      }
      $this->setStructure($this->document, $this->document->firstChild, $custom_info);
    }

    if (!empty($this->extensions)) {
      // @todo Optional LTI extensions.
    }

    if (!empty($this->launch_url)) {
      // Common Cartridge schema specifies this is actually optional.
      $url_info = $blti_defaults + array('#name' => 'launch_url', '#value' => $this->launch_url);
      $this->setStructure($this->document, $this->document->firstChild, $url_info);
    }

    if (!empty($this->secure_launch_url)) {
      // Optional secure launch url.
      $surl_info = $blti_defaults + array('#name' => 'secure_launch_url', '#value' => $this->secure_launch_url);
      $this->setStructure($this->document, $this->document->firstChild, $surl_info);
    }

    if (!empty($this->icon_url)) {
      // Optional icon url.
      $icon_info = $blti_defaults + array('#name' => 'icon', '#value' => $this->icon_url);
      $this->setStructure($this->document, $this->document->firstChild, $icon_info);
    }

    if (!empty($this->secure_icon_url)) {
      // Optional secure icon url.
      $sicon_info = $blti_defaults + array('#name' => 'secure_icon', '#value' => $this->secure_icon_url);
      $this->setStructure($this->document, $this->document->firstChild, $sicon_info);
    }

    // Add vendor information.
    $vendor_info = $blti_defaults + array('#name' => 'vendor');
    $vendor_info[] = $lticp_defaults + array('#name' => 'code', '#value' => check_plain($this->vendor['code']));
    $vendor_info[] = $lticp_defaults + array('#name' => 'name', '#value' => check_plain($this->vendor['name']));

    if (!empty($this->vendor['description'])) {
      $vendor_info[] = $lticp_defaults + array('#name' => 'description', '#value' => check_plain($this->vendor['description']));
    }
    if (!empty($this->vendor['url'])) {
      $vendor_info[] = $lticp_defaults + array('#name' => 'url', '#value' => check_plain($this->vendor['url']));
    }
    if (!empty($this->vendor['contact'])) {
      $vendor_info[] = $lticp_defaults + array(
        '#name' => 'contact',
        array(
          '#name' => 'email',
          '#namespace' => array('uri' => $this->lticpns, 'name' => 'lticp'),
          '#value' => $this->vendor['contact']['email'],
        ),
      );
    }
    $this->setStructure($this->document, $this->document->firstChild, $vendor_info);
  }

}
