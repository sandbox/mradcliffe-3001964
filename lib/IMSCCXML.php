<?php

/**
 * @file
 * IMS Global Content Package XML class.
 */

/**
 * Base class for Common Cartridge XML representation.
 *
 * This should be extended by all IMSCC XML structures such as manifests and
 * resources.
 *
 * @todo IEEE LOM metadata
 */
abstract class IMSCCXML implements IMSCCXMLInterface {

  /**
   * The DOM Document.
   *
   * @var DomDocument
   */
  public $document;

  /**
   * Initialize method.
   *
   * Creates a new IMSCCXML object from a DOMDocument.
   */
  public function __construct() {
    $this->document = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getXML() {
    return $this->document->saveXML();
  }

  /**
   * {@inheritdoc}
   */
  public static function loadDocumentFromFile($file_path) {
    try {
      $document = new DOMDocument();
      $document->load($file_path);
      $instance = new static();
      $instance->document = $document;

      $errors = $instance->validate();
      if (!empty($errors)) {
        // Throw a nicely themed exception.
        throw new InvalidArgumentException(theme('item_list', array('items' => $errors)));
      }
    }
    catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function setDocument() {
    $this->document = new DOMDocument();
  }

  /**
   * {@inheritdoc}
   */
  public function parseDocument() {
    if (!isset($this->document)) {
      return FALSE;
    }

    return $this->getStructure($this->document->firstChild);
  }

  /**
   * {@inheritdoc}
  `*/
  public function setStructure($document, $parent, $info) {

    if (isset($info['#namespace'])) {
      if (!$document->firstChild->hasAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:' . $info['#namespace']['name'])) {
        // Add new namespace uri.
        $document->firstChild->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:' . $info['#namespace']['name'], $info['#namespace']['uri']);
      }
      $element = $document->createElement($info['#namespace']['name'] . ':' . $info['#name']);
    }
    elseif (isset($info['#name'])) {
      $element = $document->createElement($info['#name']);
    }
    else {
      debug($info);
      return;
    }

    // Append the element to the parent first to avoid issues with namespace
    // redeclaration.
    $parent->appendChild($element);

    if (!empty($info['#attributes'])) {
      foreach (element_children($info['#attributes']) as $key) {
        // Create and assign attributes to element.
        $attribute = $document->createAttribute($key);
        $attribute->value = $info['#attributes'][$key];
        $element->appendChild($attribute);
      }
    }

    $children = element_children($info);

    if (!empty($children)) {
      foreach ($children as $child_name) {
        // Recurse through all element children.
        $this->setStructure($document, $element, $info[$child_name]);
      }
    }

    if (!empty($info['#value'])) {
      // Set the element value if it exists.
      $element->nodeValue = htmlentities($info['#value'], ENT_COMPAT, 'UTF-8', FALSE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStructure($element) {
    $info = array(
      '#name' => $element->nodeName,
      '#attributes' => array(),
    );

    if ($element->hasAttributes()) {
      foreach ($element->attributes as $attribute) {
        // Add to attributes array.
        $info['#attributes'][$attribute->name] = $attribute->value;
      }
    }

    if ($element->hasChildNodes()) {
      foreach ($element->childNodes as $i => $child) {
        // Recurs and add XML child as Drupal element child.
        $info[] = $this->getStructure($child);
      }
    }
    elseif (isset($element->data)) {
      $info['#value'] = html_entity_decode($element->data);
    }

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getElementByXPath($path) {
    try {
      $xpath = new DOMXPath($this->document);
      return $xpath->evaluate($path);
    }
    catch (Exception $e) {
      throw new Exception($e->getMessage());
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = array();

    if (!isset($this->document)) {
      // Don't try to validate without loading first.
      $errors[] = t('!error: There is no document loaded.', array('!error' => '<strong>Error</strong>'));
      return $errors;
    }

    // Get the defined schema from the resource or manifest implementation.
    $schema = static::schema();

    // Document must be reloaded and validated.
    $document = new DOMDocument();
    $document->loadXML($this->getXML());

    if (!empty($schema)) {
      libxml_use_internal_errors(TRUE);
      foreach ($schema as $schema_file) {
        // Validate schema.
        if (!$document->schemaValidate($schema_file)) {
          $schema_errors = libxml_get_errors();

          foreach ($schema_errors as $error) {
            $errors[] = t('@level: @code: !message (Line: @line, Col: @col)',
              array('@level' => $error->level, '@code' => $error->code, '!message' => $error->message, '@line' => $error->line, '@col' => $error->column)
            );
          }
        }
      }
      libxml_use_internal_errors(FALSE);
    }

    return $errors;
  }

}
