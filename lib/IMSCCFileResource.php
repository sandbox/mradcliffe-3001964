<?php

/**
 * @file
 * IMS Global Content Packaging Web Content Type resource class.
 */

/**
 * A base class for file-based resources.
 *
 * This includes Web Content and Associated Content types.
 *
 * Note: A file resource may have multiple files.
 *
 * @todo implement other types of content later.
 */
class IMSCCFileResource implements IMSCCResourceInterface {

  static public $name = 'webcontent';
  static public $type = 'webcontent';
  public $resource;
  public $dependency = array();
  protected $identifier;
  protected $path;
  protected $files;
  public $title;

  /**
   * Initialize method.
   *
   * @param string $identifier
   *   The identifier to use for the resource.
   */
  public function __construct($identifier = '') {
    $this->identifier = $identifier;
  }

  /**
   * Get static type.
   *
   * @return string
   *   The IMS Global Package file type.
   */
  public function getTypeAttribute() {
    return static::$type;
  }

  /**
   * {@inheritdoc}
   */
  public static function load($values) {
    $instance = new static($values['identifier']);
    $instance->setPath($values['file_path']);

    foreach ($values['files'] as $relative => $temporary) {
      // Add file to files array.
      $primary = $values['file_path'] == $relative;
      $instance->addFile($relative, $temporary, $primary);
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return $this->identifier;
  }

  /**
   * {@inheritdoc}
   */
  public function setIdentifier($identifier) {
    $this->identifier = $identifier;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_realpath($this->file_path);
  }

  /**
   * Add a file to the resource.
   *
   * @param string $path
   *   The relative path.
   * @param string $uri
   *   The file URI.
   * @param bool $primary
   *   Whether this is the resource link. Defaults to FALSE.
   */
  public function addFile($path, $uri, $primary = FALSE) {
    $this->files[$path] = $uri;

    if ($primary) {
      $this->path = $path;
    }
  }

  /**
   * Set the file resource as a file object to be saved in Drupal.
   *
   * The file should be moved using drupal_move_unmanaged_file() to the proper
   * location later.
   *
   * @param string $uri
   *   The file uri where it currently exists.
   *
   * @return object
   *   File entity object or FALSE.
   */
  public function setFileEntity($uri) {
    global $user;

    if (!drupal_realpath($uri)) {
      return FALSE;
    }

    $file = new StdClass();
    $file->fid = NULL;
    $file->uri = $uri;
    $file->filename = drupal_basename($uri);
    $file->filemime = file_get_mimetype($uri);
    $file->status = FILE_STATUS_PERMANENT;
    $file->uid = $user->uid;

    return $file;
  }

  /**
   * Gets mapping of relative to absolute/logical file path.
   *
   * @return array
   *   An associative array of files related to this resource.
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Implements IMSCCResourceInterface::createItem().
   */
  public function createItem() {
    $info = array();

    foreach (array_keys($this->files) as $path) {
      $info[] = array(
        '#name' => 'file',
        '#value' => '',
        '#attributes' => array(
          'href' => $path,
        ),
      );
    }

    if (!empty($this->dependency)) {
      foreach ($this->dependency as $identifier) {
        // Add dependency reference.
        $info[$this->identifier][] = array(
          '#name' => 'dependency',
          '#value' => '',
          '#attributes' => array(
            'identifierref' => $identifier,
          ),
        );
      }
    }

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addDependency($identifier) {
    $this->dependency[] = $identifier;
  }

}
