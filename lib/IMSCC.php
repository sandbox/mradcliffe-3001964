<?php

/**
 * @file
 * IMSCC class.
 */

/**
 * Implements IMSCCInterface and extends ArchiverZip.
 *
 * A package consists of:
 *   - An IMSCCManifest
 *   - An array of IMSCCResourceInterface resources
 *   - A file path to save the package.
 *   - A ZipArchive.
 *
 * The work flow of a package:
 *
 *   1. Create package via constructor or static method.
 *   2. Add a manifest with a unique identifier.
 *   3. Add an organization with a unique idientifer.
 *   4. Add resources.
 *   5. Add resource to organization.
 *   6. Export files, which creates DOMDocument and adds files to
 *      zip archive.
 *   7. Close the zip file for writing.
 */
class IMSCC extends ArchiverZip implements IMSCCInterface {

  /**
   * The IMSCCManifest.
   *
   * @var IMSCCManifest
   */
  public $manifest;

  /**
   * An array of IMSCCResource implemented objects.
   *
   * @var IMSCCResource[]
   */
  public $resources;

  // Full file path with stream wrapper.
  /**
   * Full file path with stream wrapper.
   *
   * @var string
   *
   * @todo deprecate and change to camel case.
   */
  public $file_path;

  // Status of archive.

  /**
   * Status of archive.
   *
   * @var bool
   */
  public $status = FALSE;

  /**
   * Extract file path.
   *
   * @var string
   *
   * @todo deprecate and change to camel case.
   */
  public $extract_path;

  /**
   * Create a new IMSCC package, which creates a new ZipArchive object.
   *
   * @param string $file_path
   *   This is unused in IMSCC extension.
   */
  function __construct($file_path = '') {
    $this->zip = new ZipArchive();

    if (!empty($file_path)) {
      $this->file_path = $file_path;
    }

    $this->resources = array();
  }

  /**
   * {@inheritdoc}
   */
  public static function loadFromFile($file_path) {
    $instance = new static($file_path);
    $instance->zip = new ZipArchive();
    $instance->file_path = $file_path;

    if (!$instance->zipOpen($file_path, 0)) {
      // Error on open.
      throw new Exception($instance->zip->getStatusString());
    }

    $contents = $instance->listContents();
    $extract_path = 'temporary://' . uniqid(trim(drupal_basename($file_path)), 'imscc');
    $instance->extract_path = $extract_path;

    // Create a temporary directory to extract to.
    drupal_mkdir($extract_path);
    $instance->extract($extract_path);

    if (!empty($contents) && in_array('imsmanifest.xml', $contents)) {
      // Load the manifest first.
      $manifest_key = array_search('imsmanifest.xml', $contents, TRUE);
      $instance->manifest = IMSCCManifest::loadDocumentFromFile($extract_path . '/' . $contents[$manifest_key]);
    }
    else {
      // A Common Cartridge MUST have a manifest file.
      throw new Exception('Error: no manifest found in common cartridge.');
    }

    try {
      // Load resources from manifest and then load the actual resource XML.
      $manifest_resources = $instance->manifest->loadResources();

      if (!empty($manifest_resources)) {
        foreach (element_children($manifest_resources) as $resource_id) {
          $resource = &$manifest_resources[$resource_id];
          $values = array(
            'file_path' => isset($resource['#attributes']['href']) ? $resource['#attributes']['href'] : $resource[0]['#attributes']['href'],
            'identifier' => $resource['#attributes']['identifier'],
            'base_path' => $extract_path,
            'files' => array(),
          );
          $type = $resource['#attributes']['type'];

          foreach (element_children($resource) as $child) {
            if ($resource[$child]['#name'] == 'file') {
              // Add to files.
              $path = $resource[$child]['#attributes']['href'];
              $values['files'][$path] = $extract_path . '/' . $path;
            }
          }

          // Load resource info by content type.
          $resource_info = imscc_get_resource_info_by_type($type);

          if (!empty($resource_info)) {
            // Provide a chance for a module to alter the class to load the
            // resource based on the structure of the resource.
            drupal_alter('imscc_resource_info_load', $resource_info, $resource);
            $class = $resource_info['class'];

            // Load resource from archive.
            $instance->resources[] = $class::load($values);
          }
        }
      }
    }
    catch (Exception $e) {
      watchdog_exception('imscc', $e, $e->getMessage(), WATCHDOG_ERROR);
    }

    $instance->zipClose();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function loadFromCache() {
    ctools_include('object-cache');
    $instance = ctools_object_cache_get('imscc', 'package', TRUE);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function cache() {
    ctools_include('object-cache');
    ctools_object_cache_set('imscc', 'package', $this);
  }

  /**
   * {@inheritdoc}
   */
  public function zipClose() {
    $closed = $this->zip->close();

    // ZipArchive is terrible at file ownership. Need to preserve permissions.
    $this->changeMode($this->file_path);

    if ($closed) {
      $this->status = FALSE;
    }

    return $closed;
  }

  /**
   * {@inheritdoc}
   */
  public function zipOpen($file_path, $mode = ZipArchive::CREATE | ZipArchive::OVERWRITE) {
    $opened = $this->zip->open(drupal_realpath($file_path), $mode);

    if ($opened) {
      $this->status = TRUE;
    }

    return $opened;
  }

  /**
   * {@inheritdoc}
   */
  public function setFileURI($uri) {
    if (file_valid_uri($uri)) {
      $this->file_path = $uri;
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function addManifest($identifier = '') {
    $this->manifest = new IMSCCManifest($identifier);
  }

  /**
   * {@inheritdoc}
   */
  public function addResource($resource) {
    $this->resources[$resource->getIdentifier()] = $resource;
  }

  /**
   * {@inheritdoc}
   */
  public function exportFiles() {
    // Open ZipArchive at file URI for writing.
    $this->zipOpen($this->file_path, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create DOMDocument for manifest.
    $this->manifest->setDocument();

    if (!empty($this->manifest->organizations)) {
      foreach (element_children($this->manifest->organizations) as $org_id) {
        // Set organization structure in the manifest document.
        $this->manifest->setOrganization($org_id);
      }
    }

    if (!empty($this->resources)) {
      foreach (element_children($this->resources) as $num) {
        // Add resource DOMDocument, add resource to manifest.
        if (method_exists($this->resources[$num], 'setDocument')) {
          // XML document resource.
          $this->resources[$num]->setDocument();
          $this->manifest->addResource($this->resources[$num]);
          $this->zip->addFromString($this->resources[$num]->getPath(), $this->resources[$num]->getXML());
        }
        else {
          // File resource.
          $this->manifest->addResource($this->resources[$num]);

          foreach ($this->resources[$num]->getFiles() as $normalized => $uri) {
            // Add the files to the archive if they exist.
            $real_path = drupal_realpath($uri);
            if ($real_path) {
              $this->zip->addFile($real_path, $normalized);
            }
          }
        }
      }
    }

    // Add manifest to archive.
    $this->zip->addFromString('imsmanifest.xml', $this->manifest->getXML());
  }

  /**
   * Recursively set file & directory permissions.
   *
   * This fixes a bug in ZipArchive where file permissions are not set the same
   * as normal file I/O in PHP.
   *
   * @param string $uri
   *   The URI scheme to the file.
   */
  protected function changeMode($uri) {
    $target = file_uri_target($uri);
    $scheme = file_uri_scheme($uri);

    if (!$target) {
      return;
    }

    // Set permission on path.
    drupal_chmod($uri);

    $paths = explode('/', $target);

    if (!empty($paths)) {
      $parent = '';
      for ($i = 0; $i < count($paths) - 1; $i++) {
        $parent .= $paths[$i] . '/';
      }

      if ($parent) {
        // Only recurse if there is a directory to do.
        $parent_uri = $scheme . '://' . $parent;
        $this->changeMode($parent_uri);
      }
    }
  }

}
