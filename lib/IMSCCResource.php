<?php

/**
 * @file
 * IMSCCResource class.
 */

/**
 * A base class for an XML-based resource.
 */
class IMSCCResource extends IMSCCXML implements IMSCCResourceInterface {

  static public $name;
  static public $type;
  public $resource;
  public $dependency = array();
  protected $identifier;
  protected $path;
  public $title;

  /**
   * {@inheritdoc}
   */
  public static function load($values) {
    $instance = static::loadDocumentFromFile($values['file_path']);
    $instance->setIdentifier($values['identifier']);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return $this->identifier;
  }

  /**
   * {@inheritdoc}
   */
  public function setIdentifier($identifier) {
    $this->identifier = $identifier;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getXML();
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->path = $path;
  }

  /**
   * {@inheritdoc}
   */
  public function createItem() {
    $info = array();

    $info[$this->identifier] = array(
      '#name' => 'file',
      '#value' => '',
      '#attributes' => array(
        'href' => $this->path,
      ),
    );

    if (!empty($this->dependency)) {
      foreach ($this->dependency as $identifier) {
        // Add dependency reference.
        $info[$this->identifier][] = array(
          '#name' => 'dependency',
          '#value' => '',
          '#attributes' => array(
            'identifierref' => $identifier,
          ),
        );
      }
    }

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addDependency($identifier) {
    $this->dependency[] = $identifier;
  }

}
