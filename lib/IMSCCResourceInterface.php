<?php

/**
 * @file
 * IMS Global Content Packinging Resource interface.
 */

/**
 * Provide a framework for implementing a Common Cartridge resource.
 */
interface IMSCCResourceInterface {

  /**
   * Load resource from file.
   *
   * @param array $values
   *   An associative array of options to pass into the resource
   *   implementation constructor:
   *     - file_path: the file path to the primary file resource.
   *     - files: an associative array of files where the key is the relative
   *       file path and the value is empty. The value should be filled in by
   *       the implementing resource.
   *     - identifier: the unique resource identifier.
   *     - base_uri: the file URI of the base extract path.
   *
   * @return IMSCCResourceInterface
   *   An IMSCCResourceInterface implementation.
   *
   * @todo add metadata to values.
   */
  public static function load($values);

  /**
   * Get identifier of resource or organization.
   *
   * @return string
   *   The unique identifier.
   */
  public function getIdentifier();

  /**
   * Set idenifier of resource.
   *
   * @param string $identifier
   *   The unique identifier to set.
   */
  public function setIdentifier($identifier);

  /**
   * Set title of resource.
   *
   * @param string $title
   *   The title of the resource.
   */
  public function setTitle($title);

  /**
   * Get title of resource.
   *
   * @return string
   *   The resource title.
   */
  public function getTitle();

  /**
   * Get resource path.
   *
   * @return string
   *   Full resource path inside package.
   */
  public function getPath();

  /**
   * Set resource path.
   *
   * @param string $path
   *   A full resource path.
   */
  public function setPath($path);

  /**
   * Create a resource item to insert into manifest.
   *
   * This is based on static member variables.
   *
   * @return array
   *   An array suitable for IMSCCXML::setStructure().
   */
  public function createItem();

  /**
   * Get the file for the resource.
   *
   * @return mixed
   *   Either an XML string or the full real path of the file.
   */
  public function getFile();

  /**
   * Add a dependency to another resource.
   *
   * @param string $identifier
   *   A resource identifier.
   */
  public function addDependency($identifier);

}
