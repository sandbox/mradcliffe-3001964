<?php

/**
 * @file
 * IMSCC Interface.
 */

/**
 * Describes an IMS Global Content Package file.
 *
 * IMS Global Content Package file format is a ZIP-compressed file, which
 * contains a manifest XML file and resources. This class extends Drupal's
 * ArchiverZip class, which creates ZipArchive objects in $zip.
 */
interface IMSCCInterface {

  /**
   * Loads an existing IMSCC package into ZipArchive by filepath.
   *
   * @param string $file_path
   *   The file path to load.
   *
   * @return IMSCC
   *   Return an IMSCC package.
   */
  public static function loadFromFile($file_path);

  /**
   * Load object from cache.
   *
   * @return IMSCC
   *   Return an IMSCC package.
   */
  public static function loadFromCache();

  /**
   * Save object to CTools cache.
   *
   * It is NOT safe to save a package to cache after exportFiles() has been
   * called.
   */
  public function cache();

  /**
   * Close ZipArchive and write file.
   *
   * @return bool
   *   TRUE if the file resource has been closed.
   */
  public function zipClose();

  /**
   * Open zip file for writing.
   *
   * @param string $file_path
   *   The File URI to open.
   * @param int $mode
   *   The mode to open the ZipArchive in.
   *
   * @return bool
   *   TRUE if the file resource has been opened.
   *
   * @see \ZipArchive
   */
  public function zipOpen($file_path, $mode = ZipArchive::CREATE | ZipArchive::OVERWRITE);

  /**
   * Set the file URI and file name on the file object.
   *
   * @param string $uri
   *   A valid URI scheme.
   */
  public function setFileURI($uri);

  /**
   * Add an empty manifest to the content package.
   *
   * @param string $identifier
   *   The unique identifier for the manifest, if it exists.
   */
  public function addManifest($identifier = '');

  /**
   * Add a resource to the content package.
   *
   * @param IMSCCResourceInterface $resource
   *   A valid IMSCCResource implemented object.
   */
  public function addResource($resource);

  /**
   * Export resources and manifest as files into archive.
   *
   * @return bool
   *   TRUE or FALSE whether ZipArchive::close() failed or not.
   */
  public function exportFiles();

}
