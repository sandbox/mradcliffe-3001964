<?php

/**
 * @file
 * IMS Global Content Package Manifest XML class.
 */

/**
 * Implements IMSCCXMLInterface and extends DOMDocument class.
 */
class IMSCCManifest extends IMSCCXML {

  public $organizations;
  public $identifier;
  public $xmlns = 'http://www.imsglobal.org/xsd/imsccv1p1/imscp_v1p1';

  /**
   * Initialize method.
   *
   * @param string $identifier
   *   Sets the identifier for the manifest.
   */
  public function __construct($identifier = '') {
    parent::__construct();

    $this->identifier = $identifier;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema() {
    $path = drupal_get_path('module', 'imscc');
    return array(
      $path . '/schema/ccv1p1_imscp_v1p2_v1p0.xsd',
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function loadDocumentFromFile($file_path) {
    $instance = parent::loadDocumentFromFile($file_path);
    $instance->loadOrganizations();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function setDocument() {
    parent::setDocument();

    if (!isset($document->firstChild)) {
      // Add manifest root element and attributes.
      $schema_location = 'http://www.imsglobal.org/xsd/imsccv1p1/imscp_v1p1 http://www.imsglobal.org/profile/cc/ccv1p1/ccv1p1_imscp_v1p2_v1p0.xsd';
      $schema_location .= ' http://ltsc.ieee.org/xsd/imsccv1p1/LOM/manifest http://www.imsglobal.org/profile/cc/ccv1p1/LOM/ccv1p1_lommanifest_v1p0.xsd';

      $manifest = $this->document->createElementNS($this->xmlns, 'manifest');
      $manifest->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', $schema_location);

      if ($this->identifier) {
        $manifest->setAttribute('identifier', $this->identifier);
      }

      // Add basic metadata, not including LOM.
      $metadata_info = array(
        '#name' => 'metadata',
        0 => array(
          '#name' => 'schema',
          '#value' => 'IMS Common Cartridge',
        ),
        1 => array(
          '#name' => 'schemaversion',
          '#value' => '1.1.0',
        ),
        2 => array(
          '#name' => 'lom',
          '#namespace' => array(
            'uri' => 'http://ltsc.ieee.org/xsd/imsccv1p1/LOM/manifest',
            'name' => 'lomimscc',
          ),
        ),
      );

      $this->document->appendChild($manifest);
      $this->setStructure($this->document, $manifest, $metadata_info);

      $organizations = $this->document->createElement('organizations');
      $resources = $this->document->createElement('resources');

      // Attach to manifest, and then append manifest, resources.
      $manifest->appendChild($organizations);
      $manifest->appendChild($resources);


    }
  }

  /**
   * Add resource to manifest.
   *
   * @param IMSCCResourceInterface $resource
   *   An IMSCCResourceInterface implementation.
   *
   * @return string
   *   The identifier for the resource.
   */
  public function addResource($resource) {
    $defaults = array(
      '#name' => 'resource',
      '#attributes' => array(
        'identifier' => $resource->getIdentifier(),
        'type' => $resource->getTypeAttribute(),
      ),
    );

    // Retrieve resource info structure wrapped in an element child.
    $info = $defaults + $resource->createItem();

    // Add element to resources section.
    foreach ($this->getElementByXPath('resources') as $parent) {
      $this->setStructure($this->document, $parent, $info);
      break;
    }

    return $resource->getIdentifier();
  }

  /**
   * Add resource directly underneath an organization hierarchy
   *
   * This satisfies the basic use case, but organization hierarchy is unlimited.
   *
   * @param string $identifier
   *   The organization identifier to add the resource to.
   * @param IMSCCResourceInterface $resource
   *   An IMSCCResourceInterface implementation.
   *
   * @return mixed
   *   Return the info array for the organization or FALSE.
   */
  public function addResourceToOrganization($identifier, $resource) {
    if (isset($this->organizations[$identifier])) {
      $this->organizations[$identifier][0][] = array(
        '#name' => 'item',
        '#attributes' => array(
          'identifier' => $identifier . '_' . $resource->getIdentifier(),
          'identifierref' => $resource->getIdentifier(),
        ),
        array(
          '#name' => 'title',
          '#value' => $resource->title,
        ),
      );

      return $this->organizations[$identifier];
    }

    return FALSE;
  }

  /**
   * Remove resource from manifest.
   *
   * @param string $identifier
   *   The identifier of the IMSCCResource to remove.
   */
  public function removeResource($identifier) {
  }

  /**
   * Load resources from manifest.
   *
   * @return array
   *   An associative array of document resource structure parsed from XML
   *   document.
   */
  public function loadResources() {
    $info = array(
      '#name' => 'resources',
    );

    // @todo Using local name here is inefficient and buggy.
    $resources = $this->getElementByXPath('//*[local-name()="resource"]');

    foreach ($resources as $resource) {
      $resource_info = $this->getStructure($resource);
      $id = $resource_info['#attributes']['identifier'];

      $info[$id] = $resource_info;
    }

    return $info;
  }

  /**
   * Add organization structure to the manifest.
   *
   * This should be called by the content package class, IMSCC.
   *
   * @param string $identifier
   *   The organization identifier to set.
   */
  public function setOrganization($identifier) {
    if (!isset($this->organizations[$identifier])) {
      return FALSE;
    }

    // Somehow the valid xpath path of /manifest/organizations does not work... PHPWTF.
    $orgs_elements = $this->getElementByXPath('organizations');

    if ($orgs_elements->length == 0) {
      throw new Exception('Could not find an organizations element in the manifest. ' . $this->document->saveXML());
    }
    $parent = $orgs_elements->item(0);

    $nodes = $this->getElementByXPath('organization[attribute::identifier="' . $identifier . '"]');

    if ($nodes->length > 0) {
      // Remove the organization tree completely and re-add it.
      $element = array_shift($nodes);
      $parent->removeChild($element);
    }

    $this->setStructure($this->document, $parent, $this->organizations[$identifier]);
  }

  /**
   * Add organization to the package.
   *
   * @param string $identifier
   *   The identifier of the organization.
   */
  public function addOrganization($identifier) {
    $this->organizations[$identifier] = array(
      '#name' => 'organization',
      '#attributes' => array(
        'identifier' => $identifier,
        'structure' => 'rooted-hierarchy',
      ),
      0 => array(
        '#name' => 'item',
        '#attributes' => array(
          'identifier' => $identifier . '_1',
        ),
      ),
    );
  }

  /**
   * Load organizations from package.
   */
  public function loadOrganizations() {
    // This is dumb, but so is XPath.
    $orgs = $this->getElementByXPath('//*[local-name()="organization"]');

    foreach ($orgs as $org) {
      $info = $this->getStructure($org);
      $id = $info['#attributes']['identifier'];

      $this->organizations[$id] = $info;
    }
  }

}
