<?php

/**
 * @file
 * IMS Global Content Packaging Web Link Content resource class.
 */

/**
 * Implements Web Link content.
 */
class IMSCCWebLink extends IMSCCResource {

  static public $type = 'imswl_xmlv1p1';
  static public $name = 'webLink';

  public $xmlns = 'http://www.imsglobal.org/xsd/imsccv1p1/imswl_v1p1';
  public $url;
  public $options;
  public $attributes = array(
    'target' => '_self',
    'href' => '',
  );

  /**
   * Get static type.
   *
   * @return string
   *   The IMS Global Package file type.
   */
  public function getTypeAttribute() {
    return self::$type;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema() {
    return array(
      drupal_get_path('module', 'imscc') . '/schema/ccv1p1_imswl_v1p1.xsd',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setDocument() {
    parent::setDocument();

    $weblink = $this->document->createElementNS($this->xmlns, 'webLink');
    $schema_location = 'http://www.imsglobal.org/xsd/imsccv1p1/imswl_v1p1 http://www.imsglobal.org/profile/cc/ccv1p1/ccv1p1_imswl_v1p1.xsd';

    $weblink
      ->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', $schema_location);

    $this->document->appendChild($weblink);

    $this->createLink($this->title, $this->url, $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadDocumentFromFile($file_path) {
    $instance = parent::loadDocumentFromFile($file_path);
    $instance->resource = $instance->parseDocument();

    return $instance;
  }

  /**
   * Set options to pass through to url().
   *
   * @param array $options
   *   An associative array of options to pass to url().
   */
  public function setOptions($options = array()) {
    $defaults = array(
      'absolute' => TRUE,
    );
    $this->options = $defaults + $options;
  }

  /**
   * Set the target of the url element.
   *
   * @param string $target
   *   A valid anchor element target such as "_new".
   */
  public function setTarget($target = '_new') {
    $this->attributes['target'] = $target;
  }

  /**
   * Set url to link to.
   *
   * @param string $url
   *   A valid path or absolute URL. This will be changed into an absolute
   *   URL later via url().
   */
  public function setURL($url) {
    $this->url = $url;
  }

  /**
   * Create link with title, url, and attributes on url element.
   */
  public function createLink() {
    global $base_url;

    $title_info = array(
      '#name' => 'title',
      '#value' => check_plain($this->title),
    );
    $this->setStructure($this->document, $this->document->firstChild, $title_info);

    // Create absolute URL.
    $this->options['absolute'] = TRUE;
    $absolute_url = url($this->url, $this->options);
    $this->attributes['href'] = $absolute_url;

    $url_info = array(
      '#name' => 'url',
      '#attributes' => $this->attributes,
    );
    $this->setStructure($this->document, $this->document->firstChild, $url_info);
  }

}
