<?php

/**
 * @file
 * IMSCC API Documentation.
 */

/**
 * IMSCC Export Package Alter.
 *
 * This provides a method of altering the IMSCC object and its contents before
 * the manifest and resource document XML is generated. Any last changes to the
 * structure of the package should be done here.
 *
 * @param IMSCCInterface $imscc
 *   The IMSCC object to alter.
 */
function hook_imscc_export_package_alter(IMSCCInterface $imscc) {

  // Example: Add a link to /user to each package.
  $weblink = imscc_create_resource('weblink');
  $weblink->setIdentifier('user-login-link');
  $weblink->setTitle('My title');
  $weblink->setUrl('user');

  $imscc->addResource($weblink);

  $organization = array_shift($imscc->manifest->organizations);
  $imscc->manifest->addResourceToOrganization($organization['#attributes']['identifier'], $weblink);
}

/**
 * IMSCC Write Package Alter.
 *
 * This provides a method of altering the IMSCC object and its contents before
 * the zip archive is closed for writing. Any last changes to the file path of
 * the package should be done here.
 *
 * @param IMSCCInterface $imscc
 *   The IMSCC object to alter.
 */
function hook_imscc_write_package_alter(IMSCCInterface $imscc) {
  // Replace imscc extension with zip extension for 1.0.0 specification.
  $imscc->file_path = preg_replace('/imscc$/', 'zip', $imscc->file_path);
}

/**
 * IMSCC Resource info load alter.
 *
 * Alter resource info for a resource when it is loaded from manifest. This is
 * the proper way of loading a resource into a different class dynamically.
 *
 * @param array &$resource_info
 *   The resource info array.
 * @param array $resource
 *   The resource structure from the manifest.
 */
function hook_imscc_resource_info_load_alter(&$resource_info, $resource) {
  if ($resource_info['type'] == 'webcontent') {
    $resource_info['class'] = 'IMSCCExampleResource';
  }
}

/**
 * IMSCC Resource info hook.
 *
 * This should not be implemented at this time.
 * Classes should also be defined in a module info file to take advantage of
 * PHP class autoload. The best way to dynamically alter what resource class is
 * used is with hook_imscc_resource_info_load_alter().
 *
 * @return array
 *   An associative array of resources keyed by a machine name. Each resource
 *   should have a class, a valid IMSCC content type, and a human-readable
 *   title defined.
 */
function hook_imscc_resource_info() {
  return array();
}

/**
 * Alter IMSCC resource info to modify what content types map to what classes.
 *
 * @param array &$imscc_resource_info
 *   An associative array of resource info provided by
 *   imscc_imscc_resource_info().
 */
function hook_imscc_resource_info_alter(&$imscc_resource_info) {
  // Alter resource info provided by other modules.
  $imscc_resource_info['webcontent']['class'] = 'IMSCCExampleResource';
}
