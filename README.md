# IMS Global Content Package API

Content Package (Common Cartridge) is an IMS Global specification to package learning content so that it may be used as a shared resource.

The imscc module currently provides support for the Common Cartridge 1.1.0 as this is the version with the greates support across Learning Management Systems.

 Feature                |  Supported   |  Planned
----------------------- | ------------ | ----------
 Web Content            |     Yes      |
 Associated Content     |      No      |    Yes
 Web Link               |     Yes      |
 LTI Link               |     Yes      |
 Discussion Topic       |      No      |    Yes
 QTI                    |      No      |    Yes
 Authorization          |      No      |     No
 Organization heirarchy |      No      |    Yes

## Usage

~~~
<?php
  // Create the package.
  $imscc = imscc_create_package('public://blah.imscc');

  // Add manifest and organization with unique identifiers.
  $imscc->addManifest('identifier');
  $imscc->manifest->addOrganization('identifier_O_1');

  // Create a resource of type weblink.
  $link = imscc_create_resource('weblink');
  $link->setIdentifier('weblink_identifier');
  $link->setPath($link->getIdentifier() . '/weblink.xml');
  $link->setTitle('User login');
  $link->setURL('user');

  // Add resource to package and manifest organization.
  $imscc->addResource($link);
  $imscc->manifest->addResourceToOrganization('identifier_O_1', $link);

  // Get resources and manifest ready for writing.
  $imscc = imscc_export_package($imscc);

  // Write package to location.
  $imscc = imscc_write_package($imscc);

  // Create a URL to the cartridge.
  $url_to_cartridge = file_create_url($imscc->file_path);

  // Create file object
  $file = imscc_create_file($imscc->file_path, NULL, 'application/zip');
  imscc_save_file($file);
?>
~~~

## Common Cartridge 1.1.0 Specification

A common cartridge is a ZIP archive with an imscc suffix.

### Manifest

A manifest is required in each Common Cartridge and is a list of organizations and resources in a package.

### Content Types

A resource may have one or more content types. Some content types are XML-based while other content types contain binary data (i.e. images, PDF, etc...).

- **Web Content**:
   - Usually HTML, CSS, JS, images, but not excluding binary files such as Word documents and PDFs.
   - All web content may be used by any resource in the common cartridge.
- **Associated Content**:
   - Represents Web Content that is scoped to a particular resource.
- **Web Link**:
   - A simple link to a fully-formed URL with a title descriptor. IMSCC module will form absolute URLs from Drupal paths automatically.
- **LTI Link**:
   - A Learning Tool Interoperability (LTI) link containing the following information from the LTI specification:
      - title
      - description
      - custom parameters
      - extensions
      - launch URL
      - secure launch URL
      - icon
      - secure icon
      - vendor
   - The Common Cartridge LTI Link does not support including OAuth Consumer information in the resource. Some consumers will not allow modifying LTI links imported via Common Cartridge.
- **Discussion Topic**:
   - This is used to initiate discussion activity, and will be created as a discussion topic created in the discussion forum of the tool.
- **QTI**:
   - Support for Question Tool Interoperability for both assessments and question banks. These should conform to the QTI 1.2.1 schema.
   - **Assessment**:
      - Contains a file element that links to a QTI XML file conforming to the QTI 1.2.1 schema.
   - **Question Bank**:
      - Contains a file element that links to a QTI XML file conforming to the QTI 1.2.1 schema.
      - Only one question bank resource may be added to a cartridge.
      - Question banks should **NOT** be included in the organization.

### Authorization

Common Cartridge authorization is not implemented per recommendation of IMS.
